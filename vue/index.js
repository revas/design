import '../core/index.scss'

import BaseBackground from './src/components/background'
import BaseButton from './src/components/button'
import BaseCard from './src/components/card'
import BaseImage from './src/components/image'
import BaseInput from './src/components/input'
import BaseLabel from './src/components/label'
import BaseList from './src/components/list'
import BaseModal from './src/components/modal'
import BaseModalCard from './src/components/modal-card'
import BaseNavbar from './src/components/navbar'
import BaseOverlay from './src/components/overlay'
import BaseSidebar from './src/components/sidebar'
import BaseTag from './src/components/tag'
import BaseIcon from './src/components/icon'

const components = {
  BaseBackground,
  BaseButton,
  BaseCard,
  BaseImage,
  BaseInput,
  BaseLabel,
  BaseList,
  BaseModal,
  BaseModalCard,
  BaseNavbar,
  BaseOverlay,
  BaseSidebar,
  BaseTag,
  BaseIcon
}

components.install = (Vue) => {
  for (const componentName in components) {
    const component = components[componentName]
    if (component && componentName !== 'install') {
      Vue.component(component.name, component)
    }
  }
}

export default components
