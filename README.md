# Revas Design System

Repositoriy of design patterns, components and resources used across the ecosystem.

## Installation

```
$ yarn add @revas/design
```

## License

All material that is not executable is released under the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/legalcode) or later. All the code is released under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0).
